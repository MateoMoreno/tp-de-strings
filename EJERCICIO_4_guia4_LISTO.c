/*
GUIA DE STRINGS - EJERCICIO 4
El usuario ingresa una palabra.
Mostrar en pantalla cuántas letras A minúsculas contiene.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{
    char palabra[21];
    int suma = 0;
    int i = 0;

    //PIDO INGRESAR LA PALABRA Y LA GUARDO
    printf("Ingrese una palabra:");
    scanf("%s", &palabra);

    //RECORRO CADA LETRA DE LA PALABRA Y LA IGUALO A LA a MINUSCULA
    //ASI SUMO CUANTAS a MINUSCULA CONTIENE LA PALABRA
    while(palabra[suma] != '\0')
    {
        if(palabra[suma] == 97)                                     //EL NUMERO 97 ES LA a MINUSCULA EN LA TABLA ASCII
        {
            i++;
        }
        suma++;
    }

    //MUESTRO EN PANTALA EL RESULTADO
    printf("\nEn la palabra hay %d letras A minusculas", i);
}
