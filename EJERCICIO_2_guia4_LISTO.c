/*
GUIA DE STRINGS - EJERCICIO 2
Permitir el ingreso de una palabra y mostrarla en pantalla al rev�s.
Por ejemplo, para "CASA" se debe mostrar "ASAC".
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{

    char palabra[21];
    int numeros = 0;
    int i;

    //PIDO INGRESAR UNA PALABRA Y LA GUARDO
    printf("Ingrese una palabra:");
    scanf("%s", &palabra);

    //CALCULA LA CANTIDAD DE LETRAS QUE TIENE LA PALABRA
    while(palabra[numeros] != '\0')
    {
        numeros++;
    }

    printf("\nPalabra al rev%cs:\n\t\t", 130);

    //MUESTRO EN PANTALLA LA PALABRA AL REV�S
    for(i=numeros; i>=0; i--)
    {
        printf("%c", palabra[i]);
    }

}
