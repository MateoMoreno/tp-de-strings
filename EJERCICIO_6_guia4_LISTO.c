/*
GUIA DE STRINGS - EJERCICIO 6
El usuario ingresa una palabra.
Determinar qu� letra aparece mayor cantidad de veces.
Para simplificar el problema, trabaje solo con may�sculas.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{

    char palabra[20];
    int letras[20];
    int top = 0, top2 = 0;
    int k = 0, q = 0, i, x;

    //PIDO INGRESAR UNA PALABRA DE 20 LETRAS MAXIMO Y EN MAYUSCULA
    printf("Escriba una palabra de 20 letras m%cximo:\n", 160);
    scanf("%s", &palabra);

    //RECORRO LETRA POR LETRA LA PALABRA Y LAS IGUALO
    for (i = 0; palabra[i] != NULL; i++)
        {
            for(x = 0; palabra[x] != NULL; x++)
            {
                if(palabra[i] == palabra[x])
                {
                    letras[i]++;
                }
            }
        }

    for (i = 0; palabra[i] != NULL; i++)
    {
        q++;
    }

    for(i = 0; i <= q; i++)
    {
        if(letras[i] == 1) k++;
    }

    //MUESTRO SI TODAS LAS LETRAS SE REPITEN LA MISMA CANTIDAD DE VECES
    if(k == q)
    {
        printf("Las letras aparecen la misma cantidad de veces en la palabra");
    }
    else
    {
        //CALCULO QUE LETRA APARECE MAS VECES EN LA PALABRA
        for(i = 1; i <= q; i++)
        {
            if(letras[i] > letras[top])
            {
                 top = i;
            }
            if(letras[i] == letras[top] && i != top && palabra[i] != palabra[top])
            {
                 top2 = i;
            }
        }

        //MOSTRAR QUE LETRA O LETRAS APARECEN MAS VECES
        if(top2 == 0)
        {
            printf("La letra que esta m%cs veces es la: '%c'", 160, palabra[top]);
        }
        else
        {
            printf("Las letras que esta m%cs veces son las: '%c' y '%c'", 160, palabra[top], palabra[top2]);
        }
    }
}
