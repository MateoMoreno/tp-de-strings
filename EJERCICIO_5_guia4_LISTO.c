/*
GUIA DE STRINGS - EJERCICIO 5
El usuario ingresa una palabra.
Mostrar en pantalla cu�ntas vocales min�sculas y may�sculas contiene.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main()
{
    char palabra[50];
    int i = 0;
    int Vmay = 0, Vmin = 0;                                        // Vmin = VOCALES MINUSCULAS y Vmax = VOCALES MAYUSCULAS

    //PIDO QUE INGRESE UNA PALABRA Y LA GUARDO
    printf("Ingrese una palabra:");
    scanf("%s", &palabra);

    //COMPARO LETRA OR LETRA CON LAS VOCALES MINUSCULAS Y MAYUSCULAS
    //HACIENDO UN CONTEO DE CUANTAS HAY EN LA PALABRA
    while(palabra[i] != '\0')
    {
        //LOS NUMEROS REPRESENTAN LAS VOCALES EN MINUSCULA (TABLA ASCII)
        if(palabra[i] == 97 || palabra[i] == 101 || palabra[i] == 105 || palabra[i] == 111 || palabra[i] == 117)
        {
            Vmin++;
        }

        //LOS NUMEROS REPRESENTAN LAS VOCALES EN MAYUSCULA (TABLA ASCII)
        if(palabra[i] == 65 || palabra[i] == 69 || palabra[i] == 73 || palabra[i] == 79 || palabra[i] == 85)
        {
            Vmay++;
        }
        i++;
    }

    //MUESTRO EL RESULTADO
    printf("\nHay %d vocales min%csculas y %d may%csculas", Vmin ,163 ,Vmay, 163);

}
