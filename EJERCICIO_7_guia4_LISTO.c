/*
GUIA DE STRINGS - EJERCICIO 7
El usuario ingresar� 5 nombres de personas y sus edades (n�mero entero).
Luego de finalizar el ingreso, muestre en pantalla el nombre de la persona m�s joven.
El ingreso se realiza de este modo: nombre y edad de la primera persona,
luego nombre y edad de la segunda, etc...
Nota: no hay que almacenar todos los nombres y todas las notas.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main()
{
    char nombre[10], persona[10];
    int edad, i, menosedad = 1000;

    //PIDO INGRESAR LOS DATOS DE LAS PERSONAS
    for(i=0; i<5; i++)
    {
        printf("\nIngrese el nombre del empleado %d:", i);
        scanf("%s", &nombre);

        printf("Ingrese la Edad del empleado %d:", i);
        scanf("%d", &edad);

        //ACA COMPARO LAS EDADES DE LAS PERSONAS Y GUARDO SIEMPRE EL NOMBRE DE LA PERSONA MAS JOVEN
        if(edad < menosedad)
        {
            strcpy(persona, nombre);
            menosedad = edad;
        }
    }
    //MUESTRO EN PANTALLA EL NOMBRE DE LA PERSONA MAS JOVEN
    printf("\nEl empleado con menos edad es %s\n", persona);
}
