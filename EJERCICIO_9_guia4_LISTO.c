/*
GUIA DE STRINGS - EJERCICIO 9
El usuario ingresar� una palabra de hasta 10 letras.
Se desea mostrarla en pantalla pero encriptada seg�n el "C�digo C�sar".
Esto consiste en reemplazar cada letra con la tercera consecutiva en el abecedario.
Por ejemplo, "CASA" se convierte en "FDVD".
Tener en cuenta que las �ltimas letras deben volver al inicio,
por ejemplo la Y se convierte B.
Este mecanismo se utilizaba en el Imperio Romano.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{
    char palabra[11];
    int i;

    //PIDO INGRESAR UNA PALABRA
    printf("Ingrese una palabra (de hasta 10 letras y en may%cscula): ", 163);
    scanf("%s", palabra);

    //ACA RECORRO LA PALABRA LETRA POR LETRA Y HAGO EN "CODIGO CESAR" DE CADA UNA
    for (i = 0; i < strlen(palabra); i++)
        {
            palabra[i] = (palabra[i] + 3);

            //CASO ESPECIAL PARA LAS ULTIMAS 3 LETRAS DEL ABECEDARIO
            if (palabra[i] > 90)
            {
                palabra[i] = palabra[i] - 26;
            }
        }
    //MUESTRO EN PANTALLA EL RESULTADO
    printf("\nPalabra encriptada seg%cn el C%cdigo C%csar: \n\t\t\t\t\t%s", 163, 162, 130, palabra);
}
