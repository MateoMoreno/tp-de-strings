/*
GUIA DE STRINGS - EJERCICIO 3
El usuario ingresa dos strings. Mostrar en pantalla si son iguales o no,
es decir, si tienen las mismas letras en las mismas posiciones.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main()
{

    char palabra1[20], palabra2[20];
    int igual = 0;
    int numeros1 = 0, numeros2 = 0;

    //PIDO INGRESAR AMBAS PALABRAS Y LAS GUARDO
    printf("Ingrese la primer palabra:");
    scanf("%s",palabra1);
    printf("Ingrese la segunda palabra:");
    scanf("%s",palabra2);

    //ACA CALCULO LA CANTIDAD DE LETRAS DE CADA PALABRA
    while(palabra1[numeros1] != '\0')
    {
        numeros1++;
    }
    while(palabra2[numeros2] != '\0')
    {
        numeros2++;
    }

    //PREGUNTO SI LA CANTIDAD DE LETRAS EN LAS PALABRAS SON DISTINTAS
    if(numeros1 != numeros2)
    {
        printf("\nLas palabras son distintas");
    }
    else
    {
        //IGUALO LETRA POR LETRA EN LAS PALABRAS HABER SI COINCIDEN TODAS
        //LUEGO MUESTRO LAS DOS OPCIONES, SI TODAS COINCIDEN O SI NO
        for (int i=0;i<=numeros1;i++)
            {
                if(palabra1[i] != palabra2[i])
                {
                     igual = 1;
                }
            }
        if(igual == 0)
        {
            printf("\nLas palabras son id%cnticas", 130);
        }
        else
        {
            printf("\nLas palabras son distintas");
        }
    }
}
