/*
GUIA DE STRINGS - EJERCICIO 8
El usuario ingresar� nombres de personas hasta que ingrese la palabra "FIN".
No sabemos cu�ntos nombres ingresar�.
Luego de finalizar el ingreso,
mostrar en pantalla cu�l es el primer nombre en orden alfab�tico de todos los ingresados.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{
    int i = 0;
    char palabra1[30];
    char palabra2[30];

    //PIDO INGRESAR LOS NOMBRES DE LA PERSONA HASTA QUE SE REGISTRE UN FIN
    while(i>=0)
    {
        printf("\nIngrese el nombre de la persona %d (Si no quiere ingresar man nombres ponga FIN): ", i);
        scanf("%s", palabra1);

        //COMPARO LAS PALABRAS CON FIN HABER SI TERMINA EL INGRESO DE NOMBRES
        if (strcmp(palabra1,"FIN") == 0)
        {
            i = -100;
        }
        else
        {
            //VEO QUE PALABRA ESTA ANTES ALFABETICAMENTE
            if (strcmp(palabra1,palabra2) < 0)
            {
                strcpy(palabra2,palabra1);
            }
        }
        i++;
    }
    //MUESTRO EN PANTALLA EL RESULTADO
    printf("\n1er Persona alfabeticamente: %s", palabra2);
}
