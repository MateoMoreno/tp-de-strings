/*
GUIA DE STRINGS - EJERCICIO 1
Permitir que el usuario ingrese una palabra de hasta 20 letras.
Mostrar en pantalla cu�ntas letras tiene la palabra ingresada.
Por ejemplo "CASA" debe indicar 4 letras.
*/

/* MATEO MORENO, 5to B ELCA TT matemoreno10@gmail.com */

#include <stdio.h>

int main ()
{
    char palabra[21];
    int numeros = 0;

    //LE PIDO A LA PERSONA INGRESAR UNA PALABRA Y LA GUARDO EN UN STRING
    printf("Ingrese una palabra (20 letras m%cximo):", 160);
    scanf("%s", &palabra);

    //ACA CUENNTO LAS LETRAS DE LA PALABRA HASTA LLEGAR AL /0 QUE ME INDICA QUE LA MISMA TERMINO
    //LUEGO MUESTRO EN PANTALLA LOS RESULTADOS
        while(palabra[numeros] != '\0')
        {
            numeros++;
        }

    printf("\nla palabra tiene %d letras", numeros);
}
